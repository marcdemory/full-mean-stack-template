// Require Libraries
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const epf = require("express-php-fpm");

// Require DB Config
const dbconfig = require('./config/database');

// Require route files
const users = require('./routes/users');
const list = require('./routes/list');


// Initialize Express
const app = express();

// Port Number
const port = process.env.PORT || 80;

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Connect to database
mongoose.connect(dbconfig.database);
mongoose.connection.on('connected', function(){
    console.log('Connected to DB');
});
mongoose.connection.on('error', function(err){
    console.log('DB Error: '+err);
});


//MIDDLEWARE
// CORS Middleware
app.use(cors());

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);


// ROUTES
//WP Route
app.get('/', (req, res) => {
    res.send("Invalid Endpoint");
});

//Users API Route
app.use('/users', users);

//List API Route
app.use('/list', list);

//Every unspecified route
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, './public/index.html'));
})


//Start Server
app.listen(port, function(){
    console.log("Server listening on port " + port);
});

