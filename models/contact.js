const mongoose = require('mongoose');

//require config
const config = require('../config/database');

//user schema
const ContactSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    belongsTo: {
        type: String,
        required: true
    }
});


//export this model
const Contact = module.exports = mongoose.model('Contact', ContactSchema);