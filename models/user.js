//require libraries
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//require config
const config = require('../config/database');

//user schema
const UserSchema = mongoose.Schema({
    name : {
        type: String
    },
    email : {
        type: String,
        required: true
    },
    username : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    }
});


//export this model
const User = module.exports = mongoose.model('User', UserSchema);


//Get a user by their ID
module.exports.getUserById = function(id, callback){
    //Finds the User with the specified ID
    User.findById(id, callback);
}

//Get a user by their username
module.exports.getUserByUsername = function(username, callback){
    //makes a JSON object with some values
    const query = {
        username : username
    };

    //Tries to find one User that has the same paramaters as the query
    User.findOne(query, callback);
}

//Add a user
module.exports.addUser = function(newUser, callback){
    bcrypt.genSalt(10, function(err, salt){
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        })
    });
}

//Compare password
module.exports.comparePassword = function(candidate, hash, callback){
    bcrypt.compare(candidate, hash, function(err, isMatch){
        if(err) throw err;
        callback(null, isMatch);
    });
}