const express = require('express');
const router = express.Router();

//require models
const Contact = require('../models/contact');

//Get all contacts
router.post('/getList', function(req,res){
    Contact.find({belongsTo: req.body.id}, function(err, resources){
        if(err){
            res.send(err).status(501);
        }else{
            res.json(resources).status(200);
        }
    });
});

//Add a contact to the List
router.post('/add', function(req,res){
    const contact = new Contact(req.body.contact);
    contact.save(function(err, resource){
        if(err){
            res.send(err).status(501);
        }else{
            res.json(resource).status(201);
        }
    });
});

//Delete a contact
router.post('/delete', function(req,res){
    const id = req.body.id;
    Contact.remove({_id:id}, function(err, resource){
        if(err){
            res.send(err);
        }else{
            res.json(resource);
        }
    });
});

//Update a contact
router.post('/update', function(req,res){
    const id = req.body.id;
    const contact = req.body.contact;

    Contact.findOneAndUpdate({_id:id}, contact, function(err, resource){
        if(err){
            res.send(err);
        }else{
            res.json(resource);
        }
    });
})

//Export router
module.exports = router;