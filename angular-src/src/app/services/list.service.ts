import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { AuthService } from './auth.service';

@Injectable()
export class ListService {

  userID: String;

  constructor(
    private http:Http,
    private authService:AuthService
  ) {
    this.userID = this.authService.getUserID();
   }


  //get contacts
  getContacts(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post("/list/getList", {id:this.userID}, {headers:headers})
      .map( res => res.json());
  }

  //save contact to database
  saveContact(contact){

    contact.belongsTo = this.userID;

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post("/list/add", {contact:contact}, {headers:headers})
      .map( res => res.json());

  }

  //delete contact from database
  deleteContact(id){
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post("/list/delete/", {id:id}, {headers:headers})
      .map( res => res.json());

  }

  //update contact in database
  updateContact(id, contact){

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post("/list/update/", {id:id, contact:contact}, {headers:headers})
      .map( res => res.json());

  }

}
