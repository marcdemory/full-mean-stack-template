import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  authToken: any;
  user: any;

  constructor( private http:Http ) { }


  //register a user
  registerUser(user){

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post('/users/register', user, {headers: headers})
      .map(res => res.json());

  }

  //authenticate a user and login
  authenticateUser(user){

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post('/users/authenticate', user, {headers: headers})
      .map(res => res.json());
  }

  //get a users profile
  getProfile(){

    this.loadToken();
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken);

    return this.http.get('/users/profile', {headers: headers})
      .map(res => res.json());

  }


  //store use data after successful login
  storeUserData( token, user){

    //Store token and user data, keep in mind local storage can only save strings
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));

    //save for the service
    this.authToken = token;
    this.user = user;

  }

  //returns the stored token 
  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  //get logged users id 
  getUserID(){
    return JSON.parse(localStorage.getItem('user')).id;
  }

  //checks if user is logged in
  loggedIn(){
    return tokenNotExpired();
  }

  //Log user out
  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

}
