import { Component, OnInit } from '@angular/core';

import { ListService } from '../../../../services/list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  formName: String;
  formEmail: String;

  contacts: Object[];

  constructor(
    private listService:ListService
  ) { }

  ngOnInit() {
    this.listService.getContacts()
      .subscribe( contacts => {
        this.contacts = contacts;
      });
  }


  //add the form contact to the list of contacts
  addContact(){
    const contact = {
      name: this.formName,
      email: this.formEmail
    };

    this.listService.saveContact(contact)
      .subscribe( data => {
        this.contacts.push(data);
        this.clearForm();
      }, 
      err =>{
        if(err){
          console.log(err);
          this.clearForm();
          return false;
        }
      });
    
  }

  //delete a contact
  deleteContact(id, index){
    this.listService.deleteContact(id)
      .subscribe( data => {
        this.contacts.splice(index, 1);
      });
  }

  //update a contact
  updateContact(id, index){
    console.log("update: " + id + ", "+ index);
    const contact = this.contacts[index];
    console.log("contact: " + JSON.stringify(contact));

    this.listService.updateContact(id, contact)
      .subscribe( data => {
        console.log(data);
      });
  }

  //clears the form
  clearForm(){
    this.formName = null;
    this.formEmail = null;
  }

  //when the user presses enter in a contact input, blur the input
  onEnter($event){
    $event.preventDefault();
    $event.target.blur();
  }

}
