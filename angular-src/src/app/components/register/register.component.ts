import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

//import services
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: String;
  username: String;
  email: String;
  password: String;

  constructor( 
    private validateService: ValidateService, 
    private flashMessages: FlashMessagesService, 
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {}


  onRegisterSubmit(){
    
    const user = {
      name: this.name,
      username: this.username,
      email: this.email,
      password: this.password
    }

    // Validate required fields
    if(!this.validateService.validateRegister(user)){
      this.flashMessages.show("Please fill in all fields", {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // Validate email
    if(!this.validateService.validateEmail(user.email)){
      this.flashMessages.show("Please use a valid email", {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // If here, everythings good, register user with auth service
    this.authService.registerUser(user)
      .subscribe( data => {
        
        if(data.success){
          this.flashMessages.show("You are successfully registered! You can now log in.", {cssClass: 'alert-success', timeout: 5000});
          this.router.navigate(['/login']);
        }else{
          this.flashMessages.show("There was a problem with your registration. Please try again.", {cssClass: 'alert-danger', timeout: 5000});
          this.router.navigate(['/register']);
        }

      });

  }

}
