import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Injectable()
export class AuthGuard implements CanActivate{

    constructor(
        private authService:AuthService,
        private router:Router,
        private flashMessages:FlashMessagesService
    ){}

    canActivate(){
        if( this.authService.loggedIn() ){
            return true;
        }else{
            this.router.navigate(['/login']);
            this.flashMessages.show("Please login first", {cssClass: "alert-info", timeout:5000});
            return false;
        }
    }

}